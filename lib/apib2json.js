/**
 * This file is part of the apib2json
 *
 * Copyright (c) 2019 Petr Bugyík
 *
 * For the full copyright and license information, please view
 * the file LICENSE.md that was distributed with this source code.
 */

const Util = require('util');
const Drafter = require('drafter.js')
const jsonContentTypeRegex = /application\/.*json.*/i;

class Apib2Json {
    /**
     * @param Object options
     */
    constructor(options = {}) {
        this.defaults = {
            verbose: false,
            pretty: false,
            raw: false,
            indent: 2,
            logger: console.log, // eslint-disable-line no-console
            drafterOptions: {
                generateSourceMap: false,
                requireBlueprintName: false,
                generateMessageBody: true,
                generateMessageBodySchema: true
            }
        };

        this.options = Object.assign(this.defaults, options);
    }

    /**
     * @param args
     */
    log(...args) {
        this.options.verbose && this.options.logger(...args);
    }

    parseGroupContent(data, group, parent)
    {
        if (group === undefined || group === null) {
            this.log('Group not defined');
        } else if (group.element === null) {
            this.log('Group element not defined');
        } else if (group.content === null) {
            this.log('Group content not defined');
        } else if (group.element === 'annotation') {
            if ('meta' in group && 'classes' in group.meta && 'content' in group.meta.classes) {
                if (group.meta.classes.element === 'array') {
                    group.meta.classes.content.forEach(content => {
                        if (content.content === 'error' && 'content' in group) {
                            throw group.content;
                        }
                    });
                }
            }
        } else if (group.element === 'httpTransaction' && Array.isArray(group.content)) {
            if (parent.element === 'transition' && parent.parent && parent.parent.element === 'resource') {
                this.parseHttpTransaction(data, parent.parent, parent, group.content);
            } else {
                this.error('Invalid structure for httpTransaction');
            }
        } else if (Array.isArray(group.content)) {
            this.log('Parsing: ' + (parent ? parent.element + '.' : '') + group.element);

            group.content.forEach(content => {
                this.log('Parsing content: ', content.element);

                let contentParent = Object.assign({}, group);
                delete contentParent.content;
                contentParent.parent = parent;
                
                this.parseGroupContent(data, content, contentParent);
            });
        } else {
            this.log('Element ' + group.element + ' has a string content');
        }
    }

    parseHttpTransaction(data, resource, transition, httpTransactions)
    {
        let endpoint = null;

        httpTransactions.forEach(httpTransaction => {
            const type = httpTransaction.element;
            if (!type in ['httpRequest', 'httpResponse']) {
                return;
            }

            const isTypeReq = type === 'httpRequest';
            if (isTypeReq) {
                endpoint = Util.format(
                    '[%s]%s',
                    httpTransaction.attributes.method.content,
                    (transition.attributes && transition.attributes.href && transition.attributes.href.content
                        ? transition.attributes.href.content
                        : resource.attributes.href.content)
                );
            }

            httpTransaction.content.forEach((rContent) => {
                const isAsset = rContent.element === 'asset';
                if (isAsset && jsonContentTypeRegex.test(rContent.attributes.contentType.content)) {
                    if (!Object.prototype.hasOwnProperty.call(data, endpoint)) {
                        data[endpoint] = [];
                    }

                    const item = {
                        meta: {
                            type: (isTypeReq ? 'request' : 'response'),
                            title: null,
                        },
                        schema: rContent.content,
                    };

                    if ('meta' in resource && 'title' in resource.meta) {
                        item.meta.group = resource.meta.title.content;
                    }

                    if ('attributes' in httpTransaction
                        && 'statusCode' in httpTransaction.attributes) {
                        item.meta.statusCode = httpTransaction.attributes.statusCode.content;
                    }

                    if ('meta' in httpTransaction && 'title' in httpTransaction.meta) {
                        item.meta.title = httpTransaction.meta.title.content;
                    }

                    data[endpoint].push(item);
                    this.log('ENDPOINT: %s %s', endpoint, JSON.stringify(item.meta));
                }
            });
        });
    }

    /**
     * @param String apib
     * @returns {Promise}
     */
    toArray(apib) {
        return new Promise((resolve, reject) => {
            const data = {};
            let raw = null;
            Drafter.parse(apib, this.options.drafterOptions, (error, result) => {
                if (error) {
                    this.log("Error while parsing blueprint: ", error);
                    reject(error);
                    return;
                }

                this.parseGroupContent(data, result, null);
                
                raw = this.options.raw ? result : null;

                resolve([data, raw]);
                this.log('DONE. No other endpoint with JSON Schema definition found.');
            });
        });
    }

    /**
     * @param String apib
     * @returns {Promise}
     */
    toJson(apib) {
        return new Promise((resolve, reject) => {
            this.toArray(apib)
                .then(([result, rawResult]) => {
                    const data = {};
                    Object.keys(result).forEach((endpoint) => {
                        const items = result[endpoint];
                        if (!Object.prototype.hasOwnProperty.call(data, endpoint)) {
                            data[endpoint] = [];
                        }

                        items.forEach((item) => {
                            const i = item;
                            i.schema = JSON.parse(i.schema);
                            data[endpoint].push(i);
                        });
                    });

                    let json = this.options.pretty
                        ? JSON.stringify(data, null, this.options.indent)
                        : JSON.stringify(data);
                    let raw = this.options.pretty
                        ? JSON.stringify(rawResult, null, this.options.indent)
                        : JSON.stringify(rawResult);
                    resolve([json, raw]);
                })
                .catch((err) => {
                    this.log("Error while parsing result: ", err);
                    reject(err);
                });
        });
    }
}

module.exports = Apib2Json;
