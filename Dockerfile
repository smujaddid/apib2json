FROM node:lts-alpine3.12
MAINTAINER Petr Bugyík

ENV PROJECT_ROOT /src/apib2json
ENV PATH ${PROJECT_ROOT}/bin:$PATH

WORKDIR ${PROJECT_ROOT}

RUN apk -U upgrade --no-cache \
    && apk -U add --no-cache \
    && apk add --no-cache --virtual .gyp \
        g++ \
        make \
        python2

ADD package.json .
ADD package-lock.json .

RUN npm install --production && \
    npm cache --force clean
RUN apk del .gyp

COPY . .

ENTRYPOINT ["apib2json"]
